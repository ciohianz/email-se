#!/usr/bin/env python
import sys
import os
import time
from helpers import HParser, EmailVerifier

class Presenter:
    pass







class Employee:

    def __init__(self, nombre, extras, domain):
        self.nombre = nombre
        self.extras = extras
        self.domain = domain

        self.guess_email()

    def guess_email(self):
        ev = EmailVerifier(self.nombre, self.domain)
        self.email = ev.find_email()
        if self.email:
            print "\tENCONTRADO: ", self.email



    def __repr__(self):
        return self.nombre




class Parser:

    employees = []

    def __init__(self, filepath, domain):
        self.filepath = filepath

        with open(self.filepath) as f:
            lines = f.readlines();

        import socks
        import socket
        socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5, "127.0.0.1", 9050, True)
        socket.socket = socks.socksocket

        for line in lines:
            nombre, aux = line.split("#", 1)
            extras = []
            for extra in aux.split("#"):
                extras.append(extra.strip());

            self.employees.append( Employee(nombre, extras, domain) )






def print_usage():
    print("Usage: email-se.py input-file domain")


if __name__ == '__main__':
    if(len(sys.argv)!=3):
        print_usage()
        sys.exit(1)

    if(not os.path.isfile(sys.argv[1])):
        print("input-file debe ser un archivo de texto")
        sys.exit(1)

    parser = Parser(sys.argv[1], sys.argv[2])

    #for employee in parser.employees:
    #    print employee