import re
from subprocess import Popen, PIPE
from emailahoy import verify_email_address
import time
import sys
import signal

def handler(signum, frame):
    sys.exit()


RE_MX = re.compile("mail\sexchanger\s=\s\d+\s(.*)\.")

class HParser:
    danger_levels = {
        'consultant': 1,
        'team-leader': 2,
        'supervisor': 3,
        'manager': 4,
        'cto|cmo|ceo|chro|president': 5,
        'devops': 6,
        'developer|tester|analyst': 7,
        'uk': 8
    }
    hierarchy_levels = {
        'president': 0,
        'cto|cmo|ceo|chro': 1,
        'manager': 2,
        'supervisor': 3,
        'team-leader': 4,
        'analyst': 5,
        'consultant': 6
    }
    dependency = {
        'cto': 'it',
        'chro': 'rrhh',
        'cmo': 'mkt|ux|ui',
        'ceo': 'support|commercial'
    }
    position = ['consultant',
                'team-leader',
                'supervisor',
                'manager',
                'cto',
                'cmo',
                'ceo',
                'chro',
                'president',
                'devops',
                'developer',
                'tester',
                'analyst',
                'uk'
            ]
    areas = ['it', 'rrhh', 'mkt', 'support', 'commercial']


class EmailVerifier:

    def __init__(self, nombre, domain):
        self.nombre = nombre
        self.domain = domain
        self.server = None

    def get_mxs(self):
        process = Popen(['nslookup', '-query=mx', self.domain], stdout=PIPE, stdin=PIPE)
        stdout, stderr = process.communicate()

        exchangers = []
        for line in stdout.splitlines():
            search = RE_MX.search(line)
            if search:
                exchangers.append(search.group(1))

        return exchangers

    def generate_server(self, mx):
        try:
            server = _smtp.SMTP(timeout=10)
            code, resp = server.connect(mx)
            if code == 220:
                return server
        except:
            pass

        return None

    def email_exists(self, email):
        found = True

        print "Trying: " + email
        mx = self.mxs[0]
        code, resp = self.server.helo(mx)
        if code != 250:
            found = False
        else:
            code, resp = self.server.mail('alan.gatticelli@icloud.com')
            if code != 250:
                found = False
            else:
                code, resp = self.server.rcpt(email)
                if code == 250:
                    print "FOUND! (%s)" % email
                else:
                    found = False
        
        return found

    def find_email(self):
        # mxs = self.get_mxs()
        # if(len(mxs)):
        #     self.mxs = mxs
        #     for mx in self.mxs:
        #         server = self.generate_server(mx)
        #         if server:
        #             self.server = server
                    

        # if not self.server:
        #     return None

        partes = self.nombre.split()
        partes_aux = partes[:]
        cant_partes = len(partes)
        email = ".".join(partes).lower()+"@"+self.domain

        parte_actual = cant_partes - 1

        signal.signal(signal.SIGINT, handler)
        print email
        encontrado = verify_email_address(email)
        reverse = 0
        while (not encontrado) and (parte_actual >= 0) :

            if(len(partes_aux[parte_actual]) > 1):
                partes_aux[parte_actual] = partes_aux[parte_actual][:-1]
            else:
                parte_actual -= 1
                partes_aux = partes[:]
                if reverse == 1:
                    partes_aux.reverse()

            if parte_actual == -1 and not reverse:
                partes_aux.reverse()
                reverse = 1
                parte_actual = cant_partes - 1

            email = ".".join(partes_aux).lower()+"@"+self.domain
            print email
            encontrado = verify_email_address(email)


        return email if encontrado else None